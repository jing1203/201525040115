package edu.mis.modules.ProductSort;

import com.jfinal.core.Controller;

/**
 * 权限控制器
 *
 */
public class ProductSortController extends Controller {
	
	static ProductSortService service = new ProductSortService();
	
	public void index() {
		render("productsort.html");
	}
	
	public void list(){
		setAttr("data", service.findAllProductSort());
		renderJson();  
	}

}


