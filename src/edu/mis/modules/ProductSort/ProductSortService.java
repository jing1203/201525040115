package edu.mis.modules.ProductSort;

import java.util.List;

import edu.mis.model.ProductSort;

/**
 * 权限服务类
 *
 */
public class ProductSortService {
	
	/**
	 * 查询所有权限
	 * @return
	 */
	public List<ProductSort> findAllProductSort(){
		return ProductSort.dao.findAll();
	}
}
