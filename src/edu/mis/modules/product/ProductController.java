package edu.mis.modules.product;

import com.jfinal.core.Controller;

import edu.mis.model.Product;
import edu.mis.model.ProductSort;


/**
 * 日志控制器
 *
 */
public class ProductController extends Controller {
	
	static ProductService service = new ProductService();
	
	/**
	 * 日志列表视图页面
	 */
	public void index() {   //如果为index，则访问地址为"路由名/" ,此处为"blog/"
		render("product.html");
	}
	
	/**
	 * 日志列表后台json
	 */
	
	public void list(){   //访问地址为 blog/list
		setAttr("data", service.findAllProductWithSort());
		renderJson();   //可直接在浏览器输入"localhost:8080/blog/list"查看返回的Json值，可以作为调试的方法
	}


	/**
	 * 添加日志
	 */
	public void save(){
		Product product  =  getModel(Product.class); //注意：使用getModel方法获取表单值非常方便，不需要对每个字段进行设置，直接得到model。注意使用此方法前端表单name必须名称为“对象名.属性名”，如blog.title   其中 blog对应Blog类，title对应Blog类的title属性
		if(service.saveProduct(product)){
			setAttr("result", true); //前端ajax回调值，进行交互判断  if(data.result){.......} 注意两个result对应
			setAttr("msg", "添加商品成功!"); //前端ajax回调值，交互消息框layer.msg(data.msg)
		}else{
			setAttr("result",false);
		}
		renderJson();  //返回json，用于向前台页面返回结果
	}
	
	public void edit() {
		setAttr("product", service.findProductById(getParaToInt("id")));  //getPara获取参数为string类型，getParaToInt将获得的String转为int
		renderJson();
	}
	
	/**
	 * 修改日志
	 */
	public void update(){
		Product product  =  getModel(Product.class);
		if(service.updateProduct(product)){
			setAttr("result", true);
			setAttr("msg", "修改商品信息成功!");
		}else{
			setAttr("result", false);
		}
		renderJson();
	}
	
	/**
	 * 删除日志
	 * 测试访问地址：localhost:8080/blog/delete?id=1
	 */
	public void delete() {
		if(service.deleteProductById(getParaToInt("id"))){
			setAttr("result", true);
			setAttr("msg", "删除商品成功!");
		}else{
			setAttr("result", false);
		}
		renderJson();
	}
	
	public void auth() {
		setAttr("product", service.findProductById(getParaToInt("id")));  //此处user应与表单中${(user.name)!}等中的user保持一致。
		setAttr("productsort",ProductSort.dao.findAll());//授权前得到所有权限
	}
	
	
	public void authorize(){
		int id=this.getParaToInt("id");   
		int sortId = this.getParaToInt("sortId");
		if(service.productAuthorize(id, sortId)){
			setAttr("result", true);
			setAttr("msg", "商品分类成功!");
		}else{
			setAttr("result", false);
		}
		renderJson();
	}
}


