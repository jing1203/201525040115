package edu.mis.modules.product;

import java.util.List;

import com.jfinal.plugin.activerecord.Record;

import edu.mis.model.Product;



/**
 * 日志服务类
 * 该类主要用于写业务逻辑（Blog没有复杂逻辑，只有调用dao），sql语句尽量不写在此类中，写在Model中
 * 如果不设计service层，则直接在controller中调用模型dao
 *
 */
public class ProductService {
	
	/**
	 * 添加日志
	 * @param blog
	 * @return
	 */
	public boolean saveProduct(Product product){  //service命名建议完整，见名知意。如此处的saveBlog，参数尽量用对象
		return product.save();
	}
	
	/**
	 * 修改日志
	 * @param blog
	 * @return
	 */
	public boolean updateProduct(Product product){
		return product.update();
	}
	
	/**
	 * 删除日志
	 * @param id
	 * @return
	 */
	public boolean deleteProductById(int id) {
		return Product.dao.deleteById(id);
	}
	
	/**
	 * 根据ID得到日志
	 * @param id
	 * @return
	 */
	public Product findProductById(int id) {
		return Product.dao.findById(id);
	}
	
	

	/**
	 * 查询所有日志
	 * @return
	 */
	/**

	/**
	 * 查询所有商品及其权限
	 * @return
	 */
	
	public List<Product> findAllProduct(){   //本项目使用dataTables支持前端分页。如果要支持后端分页，可调用Blog.dao.paginate(pageNumber, pageSize, sqlPara)
		return Product.dao.findAll();
	}
	
	public List<Record> findAllProductWithSort(){
		return Product.dao.findAllRecord();
	}

	

	public boolean productAuthorize(int id, int sortId) {
		Product product = findProductById(id);
		product.setSortId(sortId);
		return product.update();
	}
}

