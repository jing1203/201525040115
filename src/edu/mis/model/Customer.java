package edu.mis.model;

import java.util.List;

import edu.mis.model.base.BaseCustomer;

@SuppressWarnings("serial")
public class Customer extends BaseCustomer<Customer> {  //jfinal中model相当于JAVAEE框架中的DAO。baseModel相当于POJO。如果有model，重新运行代码生成器，不会重复生成model，以避免扩展的dao被覆盖
	public static final Customer dao = new Customer(); 
	/**
	 * 查询所有日志
	 * @return
	 */
	public List<Customer> findAll(){
		return dao.find("SELECT * FROM customer");
	}
}


