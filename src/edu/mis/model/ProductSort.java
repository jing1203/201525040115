package edu.mis.model;

import java.util.List;

import edu.mis.model.base.BaseProductSort;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class ProductSort extends BaseProductSort<ProductSort> {
	public static final ProductSort dao = new ProductSort().dao();
	/**
	 * 查询所有权限
	 * @return
	 */
	public List<ProductSort> findAll(){
		return dao.find("SELECT * FROM productsort");
	}
	
	/**
	 * 表关联查询_得到拥有该权限的所有用户
	 * 权限:用户为1:N关系，根据1找到所有N
	 * 参考JFINAL官方手册
	 * @return
	 */
	public List<Product> getProducts(){
		return Product.dao.find("SELECT *  FROM product WHERE sortId=?",get("id"));
	}
}
